Front End Developer
=============================

https://drupal.org/sandbox/johncionci/2221539

What Is This?
-------------

Front End Developer provides an easy to use toolkit for theme developers
that assists in the building of themes.

Features include:
* Displays current viewport resolution
* Displays common media query resolutions ( ie. Mobile, Tablet, Desktop )
* Highlights defined sidebar regions
* Highlights links that do not have a destination set
* Highlights images without alt tags set


How To Use Front End Developer
-----------------------

There are two steps to use this project:

1. Enable the module at admin/modules

2. Enable Permission at admin/people/permissions to use the
module with certain roles.

NOTE: This module should not be used on production sites.
It is a themeing and development tool only.


How To Install The Module
-------------------------

1. Install Front End Developer (unpacking it to your Drupal
/sites/all/modules directory if you're installing by hand, for example).

2. Enable Front End Developer module in Admin menu > Modules.


If you find a problem, incorrect comment, obsolete or improper code or such,
please search for an issue about it at https://drupal.org/project/issues/2221539
If there isn't already an issue for it, please create a new one.
